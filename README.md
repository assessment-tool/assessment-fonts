# Assessment Tool Fonts

Custom fonts for Assessment Tool

## Description
* This repo will compile a new font set based on SVGs in [src](src) folder
* Font preview can be found [here](https://assessment-tool.gitlab.io/assessment-fonts/)

## Requirements
1. You need [FontCustom](https://github.com/FontCustom/fontcustom) to be able to compile the font file

## Usage
* Just go into ${repolocation} and run compile

```bash
cd ~/${repolocation}
fontcustom compile
```